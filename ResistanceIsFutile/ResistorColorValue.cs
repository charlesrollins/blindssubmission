﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoustonElectronics.ResistorCalculator.Calculator
{
    /// <summary>
    /// This resistor color value class is a simple data structure that stores all possible 
    /// values for colors within the Resistor Value Matrix
    /// </summary>
    public class ResistorColorValue
    {
        /// <summary>
        /// The SignificantFigure member variable refers to the value if the band is the first,
        /// second, or possibly third index.
        /// </summary>
        public int? SignificantFigure { get; private set; }

        /// <summary>
        /// The multiplier member variable refers to the value of the third or possibly fourth
        /// index color on the resistor.  It is used to multiply by the SignificantFigure.
        /// </summary>
        public decimal? Multiplier { get; private set; }

        /// <summary>
        /// The Tolerance member variable refers to the last band of the resistor.
        /// </summary>
        public decimal? Tolerance { get; private set; }

        /// <summary>
        /// This contructor is created simply to initialize the member variables of the class.
        /// </summary>
        /// <param name="sigFigure"></param>
        /// <param name="multiplier"></param>
        /// <param name="tolerance"></param>
        public ResistorColorValue(int? sigFigure, decimal? multiplier, decimal? tolerance)
        {
            SignificantFigure = sigFigure;
            Multiplier = multiplier;
            Tolerance = tolerance;
        }
    }
}