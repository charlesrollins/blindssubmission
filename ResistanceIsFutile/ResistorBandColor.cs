﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoustonElectronics.ResistorCalculator.Calculator
{
    /// <summary>
    /// This enumeration is used to allow the application to store the colors in a clear
    /// strongly typed manner.  This way, if the language input changes or anything of
    /// the sort, these underlying colors will be secure.
    /// </summary>

    public enum ResistorBandColor
    {
        BLACK,
        BROWN,
        RED,
        ORANGE,
        YELLOW,
        GREEN,
        BLUE,
        VIOLET,
        GRAY,
        WHITE,
        GOLD,
        SILVER,
        NONE
    }
}
