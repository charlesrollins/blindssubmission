﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoustonElectronics.ResistorCalculator.Calculator
{
    /// <summary>
    /// Instead of using zero-based or one-based indexes, I decided to use an enumeration
    /// to eliminate any type of ambiguity.
    /// </summary>
    public enum BandIndex
    {
        FIRST,
        SECOND,
        THIRD,
        FOURTH,
        FIFTH
    }
}
