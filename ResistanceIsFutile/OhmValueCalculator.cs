﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoustonElectronics.ResistorCalculator.Calculator
{
    /// <summary>
    /// This OhmValueCalculator class is designed to implement the IOhmValueCalculator and expose CalculateOhmValue
    /// however it does much more.  It also supports five band resistors, tolerance, and decimal precision.
    /// </summary>
    public class OhmValueCalculator : IOhmValueCalculator
    {
        /// <summary>
        /// This matrix is the backbone of the calculator as it stores the values for all resistor color possibilities.
        /// It is made a member variable because it is used up to five times and needs only a single initialization.
        /// </summary>
        ResistorValueMatrix Matrix;

        /// <summary>
        /// What follows immediately represent the color bands of the resistor.  I strongly type them as an enumeration
        /// to ensure conformity through the applicaiton.  This was a calling process can simply use this enumeration
        /// instead of text representations
        /// </summary>
        ResistorColorValue BandA;
        ResistorColorValue BandB;
        ResistorColorValue BandC;
        ResistorColorValue BandD;
        ResistorColorValue BandE;

        /// <summary>
        /// What follows immediately are the most significant values to be determined from the resistor (the resistance
        /// and tolerance)
        /// </summary>
        decimal OhmValue;
        decimal Tolerance;

        /// <summary>
        /// What follows immediately is the designator of whether this is a four or five band resistor.  This is
        /// critically important as four and five band resistors are calculated very differently.
        /// </summary>
        bool FourBandResistor;

        /// <summary>
        /// Due to our model, bands can be added sequentially and a calculate method must be called when all bands are
        /// added to the model.  This variable allows the class to designate if the value is calculated.
        /// </summary>
        bool Calculated;

        /// <summary>
        /// Our contructor class simply initializes the matrix so that the values are readily available.
        /// </summary>
        public OhmValueCalculator()
        {
            Matrix = new ResistorValueMatrix();
        }
        
        /// <summary>
        /// This method (AddBand) below is critically important to the class as it allows the class to accumulate bands
        /// from 3 to 5 to discern the value of the resistor.
        /// </summary>
        /// <param name="color">
        /// A color must be selectd from a predefined list which represents the band.
        /// </param>
        /// <param name="index">
        /// This is the index of the band.  To eliminate confusion with zero-based or one-based indexes, I created an
        /// enumeration that forces simplicity for any calling client.
        /// </param>
        public void AddBand(ResistorBandColor color, BandIndex index)
        {
            ResistorColorValue val = Matrix[color];
            
            switch(index)
            {
                case BandIndex.FIRST:
                    BandA = val;
                    break;
                case BandIndex.SECOND:
                    BandB = val;
                    break;
                case BandIndex.THIRD:
                    BandC = val;
                    break;
                case BandIndex.FOURTH:
                    BandD = val;
                    break;
                case BandIndex.FIFTH:
                    BandE = val;
                    break;
            }
        }

        /// <summary>
        /// This is an overloaded method provided for clients that must pass strings like in the originial request
        /// </summary>
        /// <param name="color">
        /// This parameter is a string representation of the color.  It is not case-sensistive.
        /// </param>
        /// <param name="index">
        /// This parameter specifies the index using the enumeration to eliminate confusion.
        /// </param>
        public void AddBand(String color, BandIndex index)
        {
            AddBand(OhmValueCalculator.ConvertTextToColor(color), index);
        }


        /// <summary>
        /// This method (ValidateBandValues) exists only to validate the bands before calculation.
        /// </summary>
        internal void ValidateBandValues()
        {
            if(BandA == null || BandB == null || BandC == null)
            {
                throw new Exception("Incomplete information for calculation.");
            }
            
            if(BandD == null)
            {
                BandD = Matrix[ResistorBandColor.NONE];
            }

            FourBandResistor = (BandE == null);

            if(FourBandResistor)
            {
                if (BandA.SignificantFigure == null)
                    throw new FormatException("First Band was in incorrect format.");
                if (BandB.SignificantFigure == null)
                    throw new FormatException("Second Band was in incorrect format.");
                if (BandC.Multiplier == null)
                    throw new FormatException("Third Band was in incorrect format.");
                if (BandD.Tolerance == null)
                    throw new FormatException("Fourth Band was in incorrect format.");
            }
            else
            {
                if (BandA.SignificantFigure == null)
                    throw new FormatException("First Band was in incorrect format.");
                if (BandB.SignificantFigure == null)
                    throw new FormatException("Second Band was in incorrect format.");
                if (BandC.SignificantFigure == null)
                    throw new FormatException("Third Band was in incorrect format.");
                if (BandD.Multiplier == null)
                    throw new FormatException("Fourth Band was in incorrect format.");
                if (BandE.Tolerance == null)
                    throw new FormatException("Fifth Band was in incorrect format.");
            }
        }


        /// <summary>
        /// This is the core calculation method which actually yields the end results into the OhmValue and Tolerance
        /// variables.
        /// </summary>
        public void Calculate()
        {
            ValidateBandValues();

            if (FourBandResistor)
            {
                //  type casting is fine because we validated the values already
                int firstNumber = (int)BandA.SignificantFigure;
                int secondNumber = (int)BandB.SignificantFigure;

                decimal multiplier = (decimal)BandC.Multiplier;


                OhmValue = (firstNumber * 10 + secondNumber) * multiplier;
                Tolerance = (decimal)BandD.Tolerance;
            }
            else
            {
                //  type casting is fine because we validated the values already
                int firstNumber = (int)BandA.SignificantFigure;
                int secondNumber = (int)BandB.SignificantFigure;
                int thirdNumber = (int)BandC.SignificantFigure;
                decimal multiplier = (decimal)BandD.Multiplier;


                OhmValue = ((firstNumber * 100) + (secondNumber * 10) + thirdNumber) * multiplier;
                Tolerance = (decimal)BandE.Tolerance;
            }

            Calculated = true;
        }

        /// <summary>
        /// This method simply returns the value in ohms given that the calculation has already been called.
        /// </summary>
        /// <returns>
        /// This method returns a decimal representing the value in ohms for the resistor given the specified colors.
        /// </returns>
        public decimal GetOhmValue()
        {
            if(!Calculated)
            {
                throw new Exception("Value not yet calculated.");
            }
            return OhmValue;
        }

        /// <summary>
        /// This method simply returns the tolerance in percent given that the calculation has already been called.
        /// </summary>
        /// <returns>
        /// This method returns a decimal representing the tolerance.
        /// </returns>
        public decimal GetTolerance()
        {
            if (!Calculated)
            {
                throw new Exception("Value not yet calculated.");
            }
            return Tolerance;
        }


        /// <summary>
        /// This is our requested method.  It returns the value in ohms of a four band resistor.
        /// </summary>
        /// <param name="bandAColor">
        /// This parameter expects the text of the color of the first band.  It is not case-sensitive.
        /// </param>
        /// <param name="bandBColor">
        /// This parameter expects the text of the color of the second band.  It is not case-sensitive.
        /// </param>
        /// <param name="bandCColor">
        /// This parameter expects the text of the color of the third band.  It is not case-sensitive.
        /// </param>
        /// <param name="bandDColor">
        /// This parameter expects the text of the color of the fourth band.  It is not case-sensitive.
        /// </param>
        /// <returns>
        /// This method returns the value in ohms of the color code provided as an integer value.
        /// *NOTE, because a resistor can be a decimal value, this value may be rounded.
        /// </returns>
        public int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor)
        {
            if (bandAColor == null || bandBColor == null || bandCColor == null || bandDColor == null)
            {
                throw new ArgumentException("A null argument was passed");
            }

            //  Convert colors to Enum type
            AddBand(OhmValueCalculator.ConvertTextToColor(bandAColor), BandIndex.FIRST);
            AddBand(OhmValueCalculator.ConvertTextToColor(bandBColor), BandIndex.SECOND);
            AddBand(OhmValueCalculator.ConvertTextToColor(bandCColor), BandIndex.THIRD);
            AddBand(OhmValueCalculator.ConvertTextToColor(bandDColor), BandIndex.FOURTH);

            //  Put them into slots

            //  Calculate Final result
            Calculate();

            return (int)GetOhmValue();
        }
        
        /// <summary>
        /// This method converts a text string to a color enumeration.  It only accounts for a single spelling error
        /// (commonly gray is spelled grey)
        /// </summary>
        /// <param name="color">
        /// This parameter accepts colors as text.  It is not case-sensitive and currently accepts:
        /// BLACK, BROWN, RED, ORANGE, YELLOW, GREEN, BLUE, VIOLENT, GRAY, GREY, WHITE, GOLD, SILVER, NONE
        /// </param>
        /// <returns
        /// This method returns a strongly typed enumeration which is more easily used within the application.
        /// ></returns>
        internal static ResistorBandColor ConvertTextToColor(String color)
        {
            if(color == null)
            {
                throw new ArgumentException("No value was passed to ConvertTextToColor Method.");
            }


            string colorStr = color.ToUpper().Trim();

            switch(colorStr)
            {
                case "BLACK":
                    return ResistorBandColor.BLACK;
                case "BROWN":
                    return ResistorBandColor.BROWN;
                case "RED":
                    return ResistorBandColor.RED;
                case "ORANGE":
                    return ResistorBandColor.ORANGE;
                case "YELLOW":
                    return ResistorBandColor.YELLOW;
                case "GREEN":
                    return ResistorBandColor.GREEN;
                case "BLUE":
                    return ResistorBandColor.BLUE;
                case "VIOLET":
                    return ResistorBandColor.VIOLET;
                case "GRAY":
                case "GREY":
                    return ResistorBandColor.GRAY;
                case "WHITE":
                    return ResistorBandColor.WHITE;
                case "GOLD":
                    return ResistorBandColor.GOLD;
                case "SILVER":
                    return ResistorBandColor.SILVER;
                case "NONE":
                default:
                    return ResistorBandColor.NONE;
            }

            throw new FormatException("Color input was incorrect format.");
        }
    } 
}