﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoustonElectronics.ResistorCalculator.Calculator
{
    /// <summary>
    /// This interface is required by the client to be implemented by a class of the developer's choice.
    /// There is only a single function to implement (CalculateOhmValue)
    /// </summary>
    public interface IOhmValueCalculator
    {
        /// <summary>
        /// This function calculates a value of a resistor given the color componenents.
        /// </summary>
        /// <param name="bandAColor">
        /// This parameter is designated by the color  of the first band on the resistor from the left side.
        /// </param>
        /// <param name="bandBColor">
        /// This parameter is designated by the color of the second band on the resistor from the left side.
        /// </param>
        /// <param name="bandCColor">
        /// This parameter is designated by the color of the third band on the resistor from the left side.
        /// </param>
        /// <param name="bandDColor">
        /// This parameter is designated by the color of the fourth band on the resistor from the left side.
        /// </param>
        /// <returns>
        /// Returns an integer representation of the resistance meausured in ohms.
        /// *NOTE   The value can be larger than an integer can possibly hold
        /// **NOTE  The value can be a decimal number
        /// </returns>
        int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor);
    }
}
