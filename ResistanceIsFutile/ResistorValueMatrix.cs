﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoustonElectronics.ResistorCalculator.Calculator
{
    /// <summary>
    /// The ResistorValueMatrix is a simulated table of resistor colors and their
    /// possible values depending on their position.
    /// </summary>
    public class ResistorValueMatrix : Dictionary<ResistorBandColor, ResistorColorValue>
    {
        /// <summary>
        /// Currently the constructor is very crude and allows for little more than
        /// initializing the data within the class.
        /// </summary>
        public ResistorValueMatrix()
        {
            Init();
        }
        /// <summary>
        /// This init function is written to be overriden possibly by a sub-class with
        /// a different color code scheme.
        /// </summary>
        protected void Init()
        {
            this[ResistorBandColor.BLACK] = new ResistorColorValue(0, 1, null);
            this[ResistorBandColor.BROWN] = new ResistorColorValue(1, 10, 1);
            this[ResistorBandColor.RED] = new ResistorColorValue(2, 100, 2);
            this[ResistorBandColor.ORANGE] = new ResistorColorValue(3, 1000, null);
            this[ResistorBandColor.YELLOW] = new ResistorColorValue(4, 10000, 5);
            this[ResistorBandColor.GREEN] = new ResistorColorValue(5, 100000, .5m);
            this[ResistorBandColor.BLUE] = new ResistorColorValue(6, 1000000, .25m);
            this[ResistorBandColor.VIOLET] = new ResistorColorValue(7, 10000000, .1m);
            this[ResistorBandColor.GRAY] = new ResistorColorValue(8, 100000000, .05m);
            this[ResistorBandColor.WHITE] = new ResistorColorValue(9, 1000000000, null);

            this[ResistorBandColor.GOLD] = new ResistorColorValue(null, .1m, 5);
            this[ResistorBandColor.SILVER] = new ResistorColorValue(null, .01m, 10);
            this[ResistorBandColor.NONE] = new ResistorColorValue(null, null, 20);
        }
    }
}
