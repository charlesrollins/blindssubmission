﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace HoustonElectronics.ResistorCalculator.Web.Models
{


    public class ResistorCalculatorModel
    {
        public ResistorCalculatorModel()
        {
            CalculatedResult = "No result currently calculated.";
            HasFourBands = true;
        }

        [Display(Name = "Color for First Band")]
        [Required]
        public String ColorBandFirst
        {
            get;
            set;
        }

        [Display(Name = "Color for Second Band")]
        [Required]
        public String ColorBandSecond
        {
            get;
            set;
        }

        [Display(Name = "Color for Third Band")]
        [Required]
        public String ColorBandThird
        {
            get;
            set;
        }

        [Display(Name = "Color for Fourth Band")]
        [Required]
        public String ColorBandFourth
        {
            get;
            set;
        }

        [Display(Name = "Color for Fifth Band")]
        [Required]
        public String ColorBandFifth
        {
            get;
            set;
        }



        [Display(Name = "Calculated Result")]
        public String CalculatedResult
        {
            get;
            set;
        }


        [Display(Name = "Calculated Result")]
        public String MoreAccurateResult
        {
            get;
            set;
        }

        
        [Display(Name = "Only has four bands")]
        public bool HasFourBands
        {
            get;
            set;
        }


        public SelectList SignificantOnly
        {
            get
            {
                String[] significantDigitList = { "Black", "Blue", "Brown", "Gray", "Green", "Orange", "Red", "Violet", "Yellow", "White" };
                return new SelectList(significantDigitList);
            }
        }

        public SelectList MultiplierOnly
        {
            get
            {
                String[] significantDigitList = { "Black", "Blue", "Brown", "Gold", "Gray", "Green", "Red", "Silver", "Violet", "Yellow" };
                return new SelectList(significantDigitList);
            }
        }

        public SelectList ToleranceOnly
        {
            get
            {
                String[] significantDigitList = { "Blue", "Brown", "Gold", "Gray", "Green", "None", "Red", "Silver", "Violet", "Yellow"};
                return new SelectList(significantDigitList);
            }
        }

        public SelectList MultiplierOrTolerance
        {
            get
            {
                String[] significantDigitList = { "Black", "Blue", "Brown", "Gold", "Gray", "Green", "None", "Red", "Silver", "Violet", "Yellow" };
                return new SelectList(significantDigitList);
            }
        }

        public SelectList AllPossible
        {
            get
            {
                String[] significantDigitList = { "Black", "Blue", "Brown", "Gold", "Gray", "Green", "None", "Orange", "Red", "Silver", "Violet", "White", "Yellow" };
                return new SelectList(significantDigitList);
            }
        }
    }

}