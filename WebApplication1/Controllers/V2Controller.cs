﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HoustonElectronics.ResistorCalculator.Web.Models;
using HoustonElectronics.ResistorCalculator.Calculator;

namespace HoustonElectronics.ResistorCalculator.Web.Controllers
{
    public class V2Controller : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.ShowResistor = false;
            return View(new ResistorCalculatorModel());
        }

        public ActionResult V2()
        {
            ViewBag.ShowResistor = false;
            return View(new ResistorCalculatorModel());
        }

        [HttpPost]
        public ActionResult Index(ResistorCalculatorModel model)
        {
            try
            {
                OhmValueCalculator calc = new OhmValueCalculator();

                calc.AddBand(model.ColorBandFirst, BandIndex.FIRST);
                calc.AddBand(model.ColorBandSecond, BandIndex.SECOND);
                calc.AddBand(model.ColorBandThird, BandIndex.THIRD);
                calc.AddBand(model.ColorBandFourth, BandIndex.FOURTH);

                if (!model.HasFourBands)
                {
                    calc.AddBand(model.ColorBandFifth, BandIndex.FIFTH);
                }


                calc.Calculate();

                model.CalculatedResult = String.Format("The resistor specified is {0:n} Ohms and {1}% Tolerance", calc.GetOhmValue(), calc.GetTolerance());


                ViewBag.HasFourBands = model.HasFourBands;
                ViewBag.ShowResistor = true;
                ViewBag.ColorBandFirst = model.ColorBandFirst.ToUpper();
                ViewBag.ColorBandSecond = model.ColorBandSecond.ToUpper();
                ViewBag.ColorBandThird = model.ColorBandThird.ToUpper();
                ViewBag.ColorBandFourth = model.ColorBandFourth.ToUpper();
                ViewBag.ColorBandFifth = model.ColorBandFifth.ToUpper();
            }
            catch (Exception ex)
            {
                ViewBag.ShowResistor = false;
                model.CalculatedResult = String.Format("Error occurred: {0}", ex.Message);
            }

            return View(model);
        }
    }


}