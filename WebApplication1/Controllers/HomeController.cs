﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HoustonElectronics.ResistorCalculator.Web.Models;
using HoustonElectronics.ResistorCalculator.Calculator;

namespace HoustonElectronics.ResistorCalculator.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.ShowResistor = false;
            return View(new ResistorCalculatorModel());
        }

        [HttpPost]
        public ActionResult Index(ResistorCalculatorModel model)
        {
            try
            {
                OhmValueCalculator calc = new OhmValueCalculator();
                int ohmResult = calc.CalculateOhmValue(model.ColorBandFirst, model.ColorBandSecond, model.ColorBandThird, model.ColorBandFourth);

                model.CalculatedResult = String.Format("The resistor specified is {0:n0} Ohms", ohmResult);

                ViewBag.ShowResistor = true;
                ViewBag.ColorBandFirst = model.ColorBandFirst.ToUpper();
                ViewBag.ColorBandSecond = model.ColorBandSecond.ToUpper();
                ViewBag.ColorBandThird = model.ColorBandThird.ToUpper();
                ViewBag.ColorBandFourth = model.ColorBandFourth.ToUpper();


            }
            catch (Exception ex)
            {
                ViewBag.ShowResistor = false;
                model.CalculatedResult = String.Format("Error occurred: {0}", ex.Message);
            }

            return View(model);
        }
    }


}