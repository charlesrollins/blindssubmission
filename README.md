#  This repository is for blinds.com to examine my awesome programming skill.

#  To use this project, simply download all the files and run the 

#  ResistanceIsFutile.sln solution file in Visual Studio 2015

#  It currently includes:
#      a class library with the requested functionality
#      a unit test project with 27 unit tests
#      an MVC project with two interfaces

#  Enjoy!