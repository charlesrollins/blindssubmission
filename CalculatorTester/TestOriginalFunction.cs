﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HoustonElectronics.ResistorCalculator.Calculator;

namespace HoustonElectronics.ResistorCalculator.UnitTests
{
    [TestClass]
    public class TestOriginalFunction
    {

        //  This is a simple test to determine if we get an expected value from our function we were requested to write
        //  We will create many more tests for the underlying functions
        [TestMethod]
        public void TestExpectedValue01()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            Assert.AreEqual(calculator.CalculateOhmValue("green", "red", "gold", "silver"), 5);
        }

        [TestMethod]
        public void TestCaseInsensitivity()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            Assert.AreEqual(calculator.CalculateOhmValue("GrEen", "RED", "goLd", "sILVEr"), 5);
        }

        [TestMethod]
        public void TestExpectedValue02()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            Assert.AreEqual(calculator.CalculateOhmValue("yellow", "violet", "orange", "gold"), 47000);
        }

        [TestMethod]
        public void TestExpectedValue03()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            Assert.AreEqual(calculator.CalculateOhmValue("white", "violet", "black", ""), 97);
        }

        [TestMethod]
        public void TestExpectedValue04()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            Assert.AreEqual(calculator.CalculateOhmValue("yellow", "green", "orange", "silver"), 45000);
        }



        [TestMethod]
        public void TestMissingFourthBand()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            Assert.AreEqual(calculator.CalculateOhmValue("yellow", "green", "orange", ""), 45000);
        }

        [TestMethod]
        public void TestMissingFirstBand()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            try
            {
                int oVal = calculator.CalculateOhmValue("", "green", "orange", "");
            }
            catch(FormatException ex)
            {
                Assert.AreEqual(ex.Message, "First Band was in incorrect format.");
            }
        }

        [TestMethod]
        public void TestMissingSecondBand()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            try
            {
                int oVal = calculator.CalculateOhmValue("green", "", "orange", "");
            }
            catch (FormatException ex)
            {
                Assert.AreEqual(ex.Message, "Second Band was in incorrect format.");
            }
        }

        [TestMethod]
        public void TestMissingThirdBand()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            try
            {
                int oVal = calculator.CalculateOhmValue("orange", "green", "", "");
            }
            catch (FormatException ex)
            {
                Assert.AreEqual(ex.Message, "Third Band was in incorrect format.");
            }
        }

        [TestMethod]
        public void TestJunkValues01()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            try
            {
                int oVal = calculator.CalculateOhmValue("2389js", "green", "43t543q543q", "");
            }
            catch (FormatException ex)
            {
                Assert.AreEqual(ex.Message, "First Band was in incorrect format.");
            }
        }

        [TestMethod]
        public void TestJunkValues02()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            try
            {
                int oVal = calculator.CalculateOhmValue("orange", "4389asd", "yello", "");
            }
            catch (FormatException ex)
            {
                Assert.AreEqual(ex.Message, "Second Band was in incorrect format.");
            }
        }

        [TestMethod]
        public void TestJunkValues03()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();
            try
            {
                int oVal = calculator.CalculateOhmValue("orange", "green", "43t543q543q", "");
            }
            catch (FormatException ex)
            {
                Assert.AreEqual(ex.Message, "Third Band was in incorrect format.");
            }
        }

        [TestMethod]
        public void TestNullValues00()
        {
            try
            {
                OhmValueCalculator calculator = new OhmValueCalculator();
                Assert.AreEqual(calculator.CalculateOhmValue(null, "violet", "orange", "gold"), 47000);
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual(ae.Message, "A null argument was passed");
            }

        }
        [TestMethod]
        public void TestNullValues01()
        {
            try
            {
                OhmValueCalculator calculator = new OhmValueCalculator();
                Assert.AreEqual(calculator.CalculateOhmValue("violet", null, "orange", "gold"), 47000);
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual(ae.Message, "A null argument was passed");
            }

        }
        [TestMethod]
        public void TestNullValues02()
        {
            try
            {
                OhmValueCalculator calculator = new OhmValueCalculator();
                Assert.AreEqual(calculator.CalculateOhmValue("violet", "orange", null, "gold"), 47000);
            }
            catch (ArgumentException ae)
            {
                Assert.AreEqual(ae.Message, "A null argument was passed");
            }

        }
        [TestMethod]
        public void TestNullValues03()
        {
            try
            {
                OhmValueCalculator calculator = new OhmValueCalculator();
                Assert.AreEqual(calculator.CalculateOhmValue("violet", "orange", "gold", null), 47000);
            }
            catch(ArgumentException ae)
            {
                Assert.AreEqual(ae.Message, "A null argument was passed");
            }
        }

    }
}
