﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HoustonElectronics.ResistorCalculator.Calculator;

namespace CharlesRollins.Blinds
{
    [TestClass]
    public class TestFourBand
    {
        [TestMethod]
        public void TestExpectedFourBandValue20()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();

            calculator.AddBand(ResistorBandColor.YELLOW, BandIndex.FIRST);
            calculator.AddBand(ResistorBandColor.VIOLET, BandIndex.SECOND);
            calculator.AddBand(ResistorBandColor.ORANGE, BandIndex.THIRD);
            calculator.AddBand(ResistorBandColor.GOLD, BandIndex.FOURTH);

            calculator.Calculate();

            Assert.AreEqual(calculator.GetOhmValue(), 47000m);
            Assert.AreEqual(calculator.GetTolerance(), 5m);

        }

        [TestMethod]
        public void TestExpectedFourBandValue21()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();

            calculator.AddBand(ResistorBandColor.GREEN, BandIndex.FIRST);
            calculator.AddBand(ResistorBandColor.RED, BandIndex.SECOND);
            calculator.AddBand(ResistorBandColor.GOLD, BandIndex.THIRD);
            calculator.AddBand(ResistorBandColor.SILVER, BandIndex.FOURTH);

            calculator.Calculate();

            Assert.AreEqual(calculator.GetOhmValue(), 5.2m);
            Assert.AreEqual(calculator.GetTolerance(), 10m);
        }

        [TestMethod]
        public void TestExpectedFourBandValue22()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();

            calculator.AddBand(ResistorBandColor.WHITE, BandIndex.FIRST);
            calculator.AddBand(ResistorBandColor.VIOLET, BandIndex.SECOND);
            calculator.AddBand(ResistorBandColor.BLACK, BandIndex.THIRD);

            calculator.Calculate();

            Assert.AreEqual(calculator.GetOhmValue(), 97m);
            Assert.AreEqual(calculator.GetTolerance(), 20m);
        }
    }
}
