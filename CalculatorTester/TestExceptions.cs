﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HoustonElectronics.ResistorCalculator.Calculator;

namespace HoustonElectronics.ResistorCalculator.UnitTests
{
    [TestClass]
    public class TestExceptions
    {
        [TestMethod]
        public void TestWrongColorOnBand00()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();

            try
            {
                int oVal = calculator.CalculateOhmValue("gold", "red", "gold", "silver");
            }
            catch (FormatException ex)
            {
                Assert.AreEqual(ex.Message, "First Band was in incorrect format.");
            }
        }

        [TestMethod]
        public void TestWrongColorOnBand01()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();

            try
            {
                int oVal = calculator.CalculateOhmValue("red", "gold", "gold", "silver");
            }
            catch (FormatException ex)
            {
                Assert.AreEqual(ex.Message, "Second Band was in incorrect format.");
            }
        }

        [TestMethod]
        public void TestWrongColorOnBand02()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();

            try
            {
                int oVal = calculator.CalculateOhmValue("red", "yellow", "black", "silver");
            }
            catch (FormatException ex)
            {
                Assert.AreEqual(ex.Message, "Second Band was in incorrect format.");
            }

        }
        
    }
}
