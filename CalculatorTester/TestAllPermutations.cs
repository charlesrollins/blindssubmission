﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HoustonElectronics.ResistorCalculator.Calculator;

namespace HoustonElectronics.ResistorCalculator.UnitTests
{
    [TestClass]
    public class TestAllPermutations
    {
        [TestMethod]
        public void TestEverythingFourBand()
        {
            ResistorBandColor[] firstTwoBandColors = {
                ResistorBandColor.BLACK,
                ResistorBandColor.BROWN,
                ResistorBandColor.RED,
                ResistorBandColor.ORANGE,
                ResistorBandColor.YELLOW,
                ResistorBandColor.GREEN,
                ResistorBandColor.BLUE,
                ResistorBandColor.VIOLET,
                ResistorBandColor.GRAY,
                ResistorBandColor.WHITE
            };

            ResistorBandColor[] thirdBandColor =
            {
                ResistorBandColor.BLACK,
                ResistorBandColor.BROWN,
                ResistorBandColor.RED,
                ResistorBandColor.ORANGE,
                ResistorBandColor.YELLOW,
                ResistorBandColor.GREEN,
                ResistorBandColor.BLUE,
                ResistorBandColor.VIOLET,
                ResistorBandColor.GRAY,
                ResistorBandColor.WHITE,
                ResistorBandColor.GOLD,
                ResistorBandColor.SILVER
            };
            ResistorBandColor[] fourthBandColor =
            {
                ResistorBandColor.BROWN,
                ResistorBandColor.RED,
                ResistorBandColor.YELLOW,
                ResistorBandColor.GREEN,
                ResistorBandColor.BLUE,
                ResistorBandColor.VIOLET,
                ResistorBandColor.GRAY,
                ResistorBandColor.GOLD,
                ResistorBandColor.SILVER,
                ResistorBandColor.NONE
            };

            for (int i = 0; i < firstTwoBandColors.Length; ++i)
            {
                for (int j = 0; j < firstTwoBandColors.Length; ++j)
                {
                    for (int k = 0; k < thirdBandColor.Length; ++k)
                    {
                        for (int m = 0; m < fourthBandColor.Length; ++m)
                        {
                            ResistorBandColor first = firstTwoBandColors[i];
                            ResistorBandColor second = firstTwoBandColors[j];
                            ResistorBandColor third = thirdBandColor[k];
                            ResistorBandColor fourth = fourthBandColor[m];

                            OhmValueCalculator calculator = new OhmValueCalculator();
                            calculator.AddBand(first, BandIndex.FIRST);
                            calculator.AddBand(second, BandIndex.SECOND);
                            calculator.AddBand(third, BandIndex.THIRD);
                            calculator.AddBand(fourth, BandIndex.FOURTH);

                            calculator.Calculate();

                            decimal ohmValue = calculator.GetOhmValue();
                            decimal toleranceValue = calculator.GetTolerance();

                            Console.WriteLine("Values are: {0} ohms and {1} tolerance.", ohmValue, toleranceValue);
                        }
                    }
                }
            }
        }
        [TestMethod]
        public void TestEverythingFiveBand()
        {
            ResistorBandColor[] firstThreeBandColors = {
                ResistorBandColor.BLACK,
                ResistorBandColor.BROWN,
                ResistorBandColor.RED,
                ResistorBandColor.ORANGE,
                ResistorBandColor.YELLOW,
                ResistorBandColor.GREEN,
                ResistorBandColor.BLUE,
                ResistorBandColor.VIOLET,
                ResistorBandColor.GRAY,
                ResistorBandColor.WHITE
            };

            ResistorBandColor[] fourthBandColor =
            {
                ResistorBandColor.BLACK,
                ResistorBandColor.BROWN,
                ResistorBandColor.RED,
                ResistorBandColor.ORANGE,
                ResistorBandColor.YELLOW,
                ResistorBandColor.GREEN,
                ResistorBandColor.BLUE,
                ResistorBandColor.VIOLET,
                ResistorBandColor.GRAY,
                ResistorBandColor.WHITE,
                ResistorBandColor.GOLD,
                ResistorBandColor.SILVER
            };
            ResistorBandColor[] fifthBandColor =
            {
                ResistorBandColor.BROWN,
                ResistorBandColor.RED,
                ResistorBandColor.YELLOW,
                ResistorBandColor.GREEN,
                ResistorBandColor.BLUE,
                ResistorBandColor.VIOLET,
                ResistorBandColor.GRAY,
                ResistorBandColor.GOLD,
                ResistorBandColor.SILVER,
                ResistorBandColor.NONE
            };

            for (int i = 0; i < firstThreeBandColors.Length; ++i)
            {
                for (int j = 0; j < firstThreeBandColors.Length; ++j)
                {
                    for (int k = 0; k < firstThreeBandColors.Length; ++k)
                    {
                        for (int m = 0; m < fourthBandColor.Length; ++m)
                        {
                            for (int n = 0; n < fifthBandColor.Length; ++n)
                            {
                                ResistorBandColor first = firstThreeBandColors[i];
                                ResistorBandColor second = firstThreeBandColors[j];
                                ResistorBandColor third = firstThreeBandColors[k];
                                ResistorBandColor fourth = fourthBandColor[m];
                                ResistorBandColor fifth = fifthBandColor[n];

                                OhmValueCalculator calculator = new OhmValueCalculator();
                                calculator.AddBand(first, BandIndex.FIRST);
                                calculator.AddBand(second, BandIndex.SECOND);
                                calculator.AddBand(third, BandIndex.THIRD);
                                calculator.AddBand(fourth, BandIndex.FOURTH);
                                calculator.AddBand(fifth, BandIndex.FIFTH);

                                calculator.Calculate();

                                decimal ohmValue = calculator.GetOhmValue();
                                decimal toleranceValue = calculator.GetTolerance();

                                Console.WriteLine("Values are: {0} ohms and {1} tolerance.", ohmValue, toleranceValue);
                            }
                        }
                    }
                }
            }
        }
    }
}
