﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using HoustonElectronics.ResistorCalculator.Calculator;

namespace HoustonElectronics.ResistorCalculator.UnitTests
{
    [TestClass]
    public class TestFiveBand
    {
        //  Example from here http://www.allaboutcircuits.com/textbook/reference/chpt-2/resistor-color-codes/
        //  Numbers 4, 5, and 6

        [TestMethod]
        public void TestExpectedFiveBandValue10()
        { 
            OhmValueCalculator calculator = new OhmValueCalculator();

            calculator.AddBand(ResistorBandColor.ORANGE, BandIndex.FIRST);
            calculator.AddBand(ResistorBandColor.ORANGE, BandIndex.SECOND);
            calculator.AddBand(ResistorBandColor.BLACK, BandIndex.THIRD);
            calculator.AddBand(ResistorBandColor.BROWN, BandIndex.FOURTH);
            calculator.AddBand(ResistorBandColor.VIOLET, BandIndex.FIFTH);

            calculator.Calculate();

            Assert.AreEqual(calculator.GetOhmValue(), 3300m);
            Assert.AreEqual(calculator.GetTolerance(), .1m);
        }
    
        [TestMethod]
        public void TestExpectedFiveBandValue11()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();

            calculator.AddBand(ResistorBandColor.BROWN, BandIndex.FIRST);
            calculator.AddBand(ResistorBandColor.GREEN, BandIndex.SECOND);
            calculator.AddBand(ResistorBandColor.GRAY, BandIndex.THIRD);
            calculator.AddBand(ResistorBandColor.SILVER, BandIndex.FOURTH);
            calculator.AddBand(ResistorBandColor.RED, BandIndex.FIFTH);

            calculator.Calculate();

            Assert.AreEqual(calculator.GetOhmValue(), 1.58m);
            Assert.AreEqual(calculator.GetTolerance(), 2m);
        }

        [TestMethod]
        public void TestExpectedFiveBandValue12()
        {
            OhmValueCalculator calculator = new OhmValueCalculator();

            calculator.AddBand(ResistorBandColor.BLUE, BandIndex.FIRST);
            calculator.AddBand(ResistorBandColor.BROWN, BandIndex.SECOND);
            calculator.AddBand(ResistorBandColor.GREEN, BandIndex.THIRD);
            calculator.AddBand(ResistorBandColor.SILVER, BandIndex.FOURTH);
            calculator.AddBand(ResistorBandColor.BLUE, BandIndex.FIFTH);

            calculator.Calculate();

            Assert.AreEqual(calculator.GetOhmValue(), 6.15m);
            Assert.AreEqual(calculator.GetTolerance(), .25m);
        }
    }
}
